### Gestor Documental ###
Ejercicio: Se desea implementar un Gestor Documental haciendo uso de una arquitectura. 

### Solución ###

La escogida ha sido SOA. Ya que ésta nos da la facilidad de separar las funcionalidades de todo el sistema, modularización y flexibilidad.

El Modelo planteado comienza a ser el siguiente. Un placeholder inteactua con el rol de negocio "Gestor Documental". Éste interactua con los servicios de negocio. Los micro servicios de registro, de autenticación, de consulta, de creación, de visualización, y de almacenamiento. Internamente Existen Servicios y actores internos quienes controlan la autenticación y el manejo de la información. CRUD Y un almacenamiento. En la siguiente capa CRM, servicios de información de Usuario, Politicas Administrativas, los servicios, aplicaciones BD y Almacenamiento inteactuan con las interfaces y puntos de acceso de Google Cloud Platform, para acceder a su Servicio Autenticación, Bases de Datos, Funciones y Almacenamiento.

### Modelo ###

* Se encuentra en Archimate Diagrama_SOA.archimate y en Diagrama_SOA.jpg


### Integrantes ###

* 20141020053 Johan Sebastian Bonilla Plata
* 20141020096 Javier Duvan Hospital Melo
* 20141020135 Miguel Ángel Hernández Cubillos

